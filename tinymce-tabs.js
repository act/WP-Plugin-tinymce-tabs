(function() {
	tinymce.PluginManager.add( 'tabs_class', function( editor, url ) {
		// Add Button to Visual Editor Toolbar
		editor.addButton('tabs_class', {
			title: 'Insert Tabs',
			image: url + '/icon.png',
			cmd: 'tabs_class',
		});
		// Add Command when Button Clicked
		editor.addCommand('tabs_class', function() {

			// Ask the user to tab titles
			var result = prompt('Write the tabs titles, separated by a comma. Then click OK and put your text between the created tags');
			if ( !result ) {
				// User cancelled - exit
				return;
			}
			if (result.length === 0) {
				// User didn't enter a text - exit
				return;
			}

			results = result.split(",");
			var nbtabs = results.length;
			var text = '';
			for (var i = 0; i < nbtabs; i++) {
				text = text + '<br> [tabby title=\"' + results[i] +'\"] <br>' ;
			}
			text = text + '<br> [tabbyending] <br>';
			// Insert selected text back into editor, wrapping it in an anchor tag
			editor.execCommand('mceReplaceContent', false, text);
		});	
	});
})();