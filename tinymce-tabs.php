<?php
/**
 * Plugin Name: TinyMCE tabs
 * Plugin URI: http://research.reading.ac.uk/act
 * Version: 1.0
 * Author: Eric MATHIEU
 * Author URI: http://research.reading.ac.uk/act
 * Description: A simple TinyMCE Plugin to add tabs into posts or pages. Requires the plugin "Tabby Responsive Tabs"
 * License: GPL2
 */
 
 
// ==============================================
//  Prevent Direct Access of this file
// ==============================================

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if this file is accessed directly

 
class TinyMCE_Tabs_Class  {
	
	/**
	* Constructor. Called when the plugin is initialised.
	*/
	function __construct() {
		if ( is_admin() ) {
			add_action( 'init', 'setup_tinymce_plugin' );
		}
		
		
	}

}

$tinymce_tabs = new TinyMCE_Tabs_Class;


/**
* Check if the current user can edit Posts or Pages, and is using the Visual Editor
* If so, add some filters so we can register our plugin
*/
function setup_tinymce_plugin() {
	// Check if the logged in WordPress User can edit Posts or Pages
	// If not, don't register our TinyMCE plugin
		
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
				return;
	}

	// Check if the logged in WordPress User has the Visual Editor enabled
	// If not, don't register our TinyMCE plugin
	if ( get_user_option( 'rich_editing' ) !== 'true' ) {
	return;
	}

	// Setup some filters
	add_filter( 'mce_external_plugins', 'add_tinymce_plugin2'  );
	add_filter( 'mce_buttons', 'add_tinymce_toolbar_button'  );
}

/**
* Adds a TinyMCE plugin compatible JS file to the TinyMCE / Visual Editor instance
*
* @param array $plugin_array Array of registered TinyMCE Plugins
* @return array Modified array of registered TinyMCE Plugins
*/
function add_tinymce_plugin2( $plugin_array ) {
	$plugin_array['tabs_class'] = plugin_dir_url( __FILE__ ) . 'tinymce-tabs.js';
	return $plugin_array;
}

/**
* Adds a button to the TinyMCE / Visual Editor which the user can click
* to insert a link with a custom CSS class.
*
* @param array $buttons Array of registered TinyMCE Buttons
* @return array Modified array of registered TinyMCE Buttons
*/
function add_tinymce_toolbar_button( $buttons ) {
	array_push( $buttons, '|', 'tabs_class' );
	return $buttons;
}
