 * Author URI: http://research.reading.ac.uk/act
 * Description: A simple TinyMCE Plugin to add tabs into posts or pages. Requires the plugin "Tabby Responsive Tabs"
 * License: GPL2